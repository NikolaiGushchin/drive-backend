import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";

import { UserEntity } from "src/users/entities/user.entity";
import { UsersRepository } from "src/users/users.repository";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET_KEY'),
      ignoreExpiration: true,
    } as StrategyOptions)
  }

  async validate(payload: { sub: UserEntity['id'] }): Promise<UserEntity> {
    const userId = payload.sub;

    return this.usersRepository.findOneOrFail(userId);
  }
}
