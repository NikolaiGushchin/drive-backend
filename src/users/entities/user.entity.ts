import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

import { BaseEntity } from "src/common/entities/base.entity";
import { FileEntity } from "src/file/entities/file.entity";


const tableName = 'users';

@Entity({ name: tableName })
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @OneToMany(() => FileEntity, (file) => file.user)
  files?: FileEntity[];
}
