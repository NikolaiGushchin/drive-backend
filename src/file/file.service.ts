import { BadRequestException, Injectable } from '@nestjs/common';
import { DeleteFileResponseDto, FileResponseDto } from './dto/file.response.dto';
import { path } from 'app-root-path'
import { ensureDir, writeFile, remove } from 'fs-extra';
import { UserEntity } from 'src/users/entities/user.entity';
import { FileEntity } from './entities/file.entity';
import { FileRepository } from './file.repository';
import { v4 } from 'uuid'

@Injectable()
export class FileService {
  constructor(private readonly filesRepository: FileRepository) {}

  async getOneFile(
    userId: UserEntity['id'],
    fileId: FileEntity['id'],
  ): Promise<FileResponseDto> {
    const file = await this.filesRepository.findOneOrFail({
      where: {
        id: fileId,
        userId: userId,
      }
    });

    return {
      fileUrl: file.fileUrl,
      name: file.name,
    }
  }

  async getAllFiles(
    id: UserEntity['id']
  ): Promise<FileEntity[]> {
    return this.filesRepository.find({ where: {
      userId: id,
    } });
  }

  async createFiles(
    id: UserEntity['id'],
    file: Express.Multer.File, 
    folder: string = 'def',
  ): Promise<FileEntity> {
    const uploadFile = await this.saveFiles(file, id);

    return await this.filesRepository.save({ 
      name: uploadFile.name,
      fileUrl: uploadFile.fileUrl,
      userId: id,
    });
  }

  private async saveFiles(
    file: Express.Multer.File, 
    folder: string = 'def'
  ): Promise<FileResponseDto> {
    const loadsFolder = `${path}/uploads/${folder}`;

    await ensureDir(loadsFolder);

    const nameFile = `${v4() + '-' + file.originalname}`;

    await writeFile(`${loadsFolder}/${nameFile}`, file.buffer);
    
    return {
      fileUrl: `uploads/${folder}/${nameFile}`,
      name: nameFile,
    }
  }

  async deleteFile(
    userId: UserEntity['id'],
    fileId: FileEntity['id'],
  ): Promise<DeleteFileResponseDto> {
    const delFile = await this.filesRepository.findOneOrFail({
      where: {
        id: fileId,
        userId: userId,
      }
    });

    if (!delFile) {
      throw new BadRequestException('Неверный id пользователя или файла');
    }

    await this.removeFile(delFile.fileUrl);

    await this.filesRepository.delete(delFile.id);

    return {
      id: delFile.id,
    };
  }

  private async removeFile(fileUrl: string): Promise<boolean> {
    await remove(fileUrl);

    return true;
  }
}
