import { Controller, Delete, Get, HttpStatus, Param, Post, Query, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiParam, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { User } from 'src/users/decorators/user.decorator';
import { UserEntity } from 'src/users/entities/user.entity';
import { DeleteFileResponseDto, FileResponseDto } from './dto/file.response.dto';
import { FileEntity } from './entities/file.entity';
import { FileService } from './file.service';

@ApiBearerAuth('JWT')
@ApiTags('files')
@UseGuards(JwtAuthGuard)
@Controller('files')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @ApiParam({
    name: 'id',
    type: String,
  })
  @Get('uploads/:userId/:fileName')
  async getOneFileById(
    @User('id') userId: UserEntity['id'],
    @Param('fileName') fileName: FileEntity['name'],
    @Res() res,
  ) {
    const response = res.sendFile(fileName, { root: `./uploads/${userId}/` });
    return {
      status: HttpStatus.OK,
      data: response,
    }
  }

  @Get()
  async getAllFilesByUserId(
    @User('id') userId: UserEntity['id']
  ): Promise<FileResponseDto[]> {
    return this.fileService.getAllFiles(userId);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Post('uploads')
  @UseInterceptors(FileInterceptor('file'))
  async uploadsFile(
    @User('id') userId: UserEntity['id'],
    @UploadedFile() file: Express.Multer.File,
    @Query('folder') folder?: string
  ): Promise<FileEntity> {
    return this.fileService.createFiles(userId, file, folder);
  }

  @ApiParam({
    name: 'id',
    type: String,
  })
  @Delete(':id')
  async deleteFileById(
    @User('id') userId: UserEntity['id'],
    @Param('id') fileId: FileEntity['id'], 
  ): Promise<DeleteFileResponseDto> {
    return this.fileService.deleteFile(userId, fileId);
  }
}
