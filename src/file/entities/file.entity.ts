import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

import { BaseEntity } from "src/common/entities/base.entity";
import { UserEntity } from "src/users/entities/user.entity";


const tableName = 'files'

@Entity({ name: tableName })
export class FileEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  
  @Column()
  name: string;

  @Column()
  fileUrl: string;

  @Column()
  userId: string;

  @ManyToOne(() => UserEntity, (user) => user.files)
  user: UserEntity;
}
