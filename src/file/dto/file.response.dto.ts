import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class FileResponseDto {
  @ApiProperty()
  @IsString()
  fileUrl: string;
  
  @ApiProperty()
  @IsString()
  name: string;
}

export class DeleteFileResponseDto {
  id: string;
}