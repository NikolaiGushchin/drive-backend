import { Module } from '@nestjs/common';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import { ServeStaticModule } from '@nestjs/serve-static'
import { path } from 'app-root-path'
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileRepository } from './file.repository';
import { UsersRepository } from 'src/users/users.repository';


@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: `${path}/uploads`,
      serveRoot: '/uploads',
    }),

    TypeOrmModule.forFeature([ FileRepository, UsersRepository ]),
  ],
  providers: [FileService],
  controllers: [FileController],
  exports: [FileService],
})
export class FileModule {}
