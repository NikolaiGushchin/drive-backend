import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle('Drive')
    .setDescription('The drive API description')
    .setVersion('1.0')
    .addTag('drive')
    .addBearerAuth({
      in: 'header',
      type: 'http',
      bearerFormat: 'JWT'
    }, 'JWT')  
    .build();
    
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.useGlobalPipes(new ValidationPipe());
  
  app.enableCors();
  
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
